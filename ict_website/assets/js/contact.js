
 // Initialize Firebase
  var config = {
    apiKey: "AIzaSyAoIdBQB81-KtNbnzTONAbDfS6u0WapNK8",
    authDomain: "ictcodes-website.firebaseapp.com",
    databaseURL: "https://ictcodes-website.firebaseio.com",
    projectId: "ictcodes-website",
    storageBucket: "ictcodes-website.appspot.com",
    messagingSenderId: "808229068986"
  };
  firebase.initializeApp(config);

  
 var messagesRef = firebase.database().ref('messages');
  
 document.getElementById('contactForm').addEventListener('submit', submitForm);
  
function submitForm(e){
  e.preventDefault();
  
  var name = getInputVal('name');
  var email = getInputVal('email');
  var phone = getInputVal('phone');
  var message = getInputVal('message');
  
  saveMessage(name, email, phone, message);
   
    // Show alert
  document.querySelector('.alert').style.display = 'block';

  // Hide alert after 3 seconds
  setTimeout(function(){
    document.querySelector('.alert').style.display = 'none';
  },3000);

  // Clear form
  document.getElementById('contactForm').reset();
}
// Function to get get form values
function getInputVal(id){
  return document.getElementById(id).value;
}

// Save message to firebase
function saveMessage(name, email, phone, message){
  var newMessageRef = messagesRef.push();
  newMessageRef.set({
    name: name,
    email:email,
    phone:phone,
    message:message
  });
}