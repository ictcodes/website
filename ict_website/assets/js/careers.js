 var config = {
    apiKey: "AIzaSyAoIdBQB81-KtNbnzTONAbDfS6u0WapNK8",
    authDomain: "ictcodes-website.firebaseapp.com",
    databaseURL: "https://ictcodes-website.firebaseio.com",
    projectId: "ictcodes-website",
    storageBucket: "ictcodes-website.appspot.com",
    messagingSenderId: "808229068986"
  };
  firebase.initializeApp(config);

  
 var messagesRef = firebase.database().ref('training');
  
 document.getElementById('trainingForm').addEventListener('submit', submitForm);
  
function submitForm(e){
  e.preventDefault();
  
  var firstname = getInputVal('firstName');
  var lastname= getInputVal('lastName');
  var email = getInputVal('email');
  var phone = getInputVal('phone');
  var college = getInputVal('college');
  var department = getInputVal('department');
  
  saveMessage(firstname, lastname, email, phone, college, department);
   
    // Show alert
  document.querySelector('.alert').style.display = 'block';

  // Hide alert after 3 seconds
  setTimeout(function(){
    document.querySelector('.alert').style.display = 'none';
  },10000);

  // Clear form
  document.getElementById('trainingForm').reset();
}
// Function to get get form values
function getInputVal(id){
  return document.getElementById(id).value;
}

// Save message to firebase
function saveMessage(firstname, lastname, email, phone, college, department){
  var newMessageRef = messagesRef.push();
  newMessageRef.set({
    firstname: firstname,
    lastname: lastname,
    email:email,
    phone:phone,
    college: college,
    department:department
  });
}